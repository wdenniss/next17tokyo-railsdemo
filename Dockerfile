FROM ruby:2.4.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs imagemagick
RUN gem install bundler
RUN bundle config --global silence_root_warning 1 # ref: https://github.com/docker-library/rails/issues/10
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
