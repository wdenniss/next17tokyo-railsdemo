Rails.application.routes.draw do

  get 'resize', to: 'application#resize'

  root 'application#index'
  
end
