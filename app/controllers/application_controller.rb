class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception

  def index
    #data = File.open(Rails.root.join('public/index.html')).read
    #send_data data, :type => 'text/html', :disposition => 'inline'
    render "index"
  end

  def resize

    image = MiniMagick::Image.open(Rails.root.join('public/PIA13316.jpg'))
    image.resize "3000x3000"
    
    send_data image.to_blob, :type => 'image/png	', :disposition => 'inline'

  end

end
